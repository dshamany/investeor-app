import React from 'react';
import { useNavigate } from 'react-router-dom';
import './startup.style.css';

export const StartupView = () => {
    const navigate = useNavigate();

    React.useEffect(() => {
        const token = localStorage.getItem("token")
        if (!token){
            navigate("/signin");
        }
    });

    return (
        <div className="startup-view">
            <h1>Startup View</h1>
        </div>
    )
}

export default StartupView;