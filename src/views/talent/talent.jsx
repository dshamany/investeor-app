import React from 'react';
import { useNavigate } from 'react-router-dom';
import "./talent.style.css";

export const TalentView = () => {

    const navigate = useNavigate();

    React.useEffect(() => {
        const token = localStorage.getItem("token")
        if (!token){
            navigate("/signin");
        }
    });

    return (
        <div className="talent-view">
            <h1>Talent View</h1>
        </div>
    )
}

export default TalentView;