import React from 'react';
import "./account.style.css";

import { Button, Form } from 'react-bootstrap';

import { Spinner } from '../../components/spinner';
import { getUser, updateUser } from '../../lib/api';

export const AccountView = () => {

    const [loading, setLoading] = React.useState(false);

    const [userInfo, setUserInfo] = React.useState({});

    const [fullname, setFullname] = React.useState("");
    const [email, setEmail] = React.useState("");
    const [portfolioUrl, setPortfolioUrl] = React.useState("");
    const [gitUrl, setGitUrl] = React.useState("");
    const [bio, setBio] = React.useState("");

    // eslint-disable-next-line
    React.useEffect(() => {
        setLoading(true);
        const token = localStorage.getItem("token");
        getUser(token, (userItem) => {
            userItem = userItem["user"];

            setFullname(userItem["fullname"]);
            setEmail(userItem["email"]);
            setPortfolioUrl(userItem["portfolioUrl"]);
            setGitUrl(userItem["gitUrl"]);
            setBio(userItem["bio"]);

            setUserInfo({
                fullname: fullname,
                email: email,
                portfolioUrl: portfolioUrl,
                gitUrl: gitUrl,
                bio: bio,
                photoUrl: userItem["photoUrl"]
            });

            setLoading(false);
        });

    }, []);


    return (
        <div className="account-container container mt-3 pt-5">
            { !loading && <div className='container'>
                <div className="row">
                    <div className="col-5 m-3">
                        <img src={userInfo && userInfo["photoUrl"] ? userInfo["photoUrl"] : "https://tekeye.uk/md_cms/images/placeholder-250px.png"} width={300} height={400} alt="profile" />
                    </div>
                    <div className="col-6">
                        <textarea className='m-3' placeholder='Enter your biography here' value={bio} onChange={(e) => setBio(e.target.value)}>
                        </textarea>
                    </div>
                    <div className="col-8 m-3 w-100">
                        <Form>
                            <Form.Control value={fullname} onChange={(e) => setFullname(e.target.value)} type="text" placeholder='Fullname' />
                            <Form.Control value={email} onChange={(e) => setEmail(e.target.value)} type="text" placeholder='Email' />
                            <Form.Control value={portfolioUrl} onChange={(e) => setPortfolioUrl(e.target.value)} type="text" placeholder="Portfolio Website" />
                            <Form.Control value={gitUrl} onChange={(e) => setGitUrl(e.target.value)} type="text" placeholder="Github or GitLab" />
                        </Form>
                    </div>
                </div>
                <div className="row">
                    <div className="col"></div>
                    <div className="col">
                        <Button onClick={() => {
                            setUserInfo({
                                fullname: fullname,
                                email: email,
                                portfolioUrl: portfolioUrl,
                                gitUrl: gitUrl,
                                bio: bio
                            });

                            let payload = {
                                "email": userInfo["email"],
                                "update-attributes": {
                                    fullname,
                                    portfolioUrl,
                                    gitUrl,
                                    bio
                                }
                            }

                            updateUser(payload, (res) => console.log(res));

                        }} className="primary" type="button">Save</Button>
                    </div>
                </div>
            </div>}
            { loading && <div style={{ width: "100%", height: "100%", display: "flex", justifyContent: "center", alignItems: "center"}}><Spinner /></div> }
        </div>
    )
}

export default AccountView;