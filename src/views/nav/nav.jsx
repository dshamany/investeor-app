import React, { useState } from 'react';
import "./nav.style.css";
import Logo from "../../investeor-colored.png";

import { useNavigate } from 'react-router-dom';

import Offcanvas from 'react-bootstrap/Offcanvas';

export const Nav = (props) => {
    const [fullName, setFullname] = useState();
    const [show, setShow] = useState(false);

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    const userInfo = () => {
        let user = JSON.parse(localStorage.getItem("user"));

        if (!user) {
            return null;
        }

        return user;
    }

    const listIcon = (props) => (
        <svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" fill={props.color} class="bi bi-list" viewBox="0 0 16 16">
            <path fill-rule="evenodd" d="M2.5 12a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5z" />
        </svg>
    )

    const nav = useNavigate();
    const navigate = (path) => {
        handleClose();
        nav(path);
    }

    // This allows the fullname to render properly
    // when signing in and out of the app
    // so we ignore the warning here
    // eslint-disable-next-line
    React.useEffect(() => {
        let user = userInfo();
        if (user){
            setFullname(user["fullname"]);
        } else {
            setFullname("");
        }
    });

    return (
        <div className="navigation-container">
            <div className="top-bar">
                <div className='menu' onClick={handleShow}>{listIcon({ color: "lightgray" })}</div>
                <img onClick={() => navigate("/")} src={Logo} width="150px" alt="Investeor Logo" style={{margin: "0 15px", cursor: "pointer" }} />
                <div className="account" onClick={() => navigate("/account")}>{fullName}</div>
            </div>
            <Offcanvas className="sm" show={show} onHide={handleClose}>
                <Offcanvas.Body className="offcanvas-body">
                    <ul>
                    {userInfo() && <li onClick={() => navigate("/")}>News</li>}
                    {userInfo() && <li onClick={() => navigate("/investor")}>Find an Investor</li>}
                    {userInfo() && <li onClick={() => navigate("/startup")}>Become a Startup</li>}
                    {userInfo() && <li onClick={() => navigate("/talent")}>Join a Startup</li>}
                    {!userInfo() && <li id="signup-nav-btn" onClick={() => navigate("/signin")}>Sign In or Sign Up</li>}
                    {userInfo() && <li id="signup-nav-btn" onClick={() => {
                        localStorage.removeItem("user");
                        localStorage.removeItem("token");
                        navigate("/signin");
                    }}>Sign Out</li>}
                    </ul>
                </Offcanvas.Body>
            </Offcanvas>
            {props.children}
        </div>
    )
};

export default Nav;