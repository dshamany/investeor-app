import React from 'react';
import { useNavigate } from 'react-router-dom';
import { InvestorGrid } from '../../components/investorGrid';
import './investor.style.css';

export const InvestorView = () => {
    const navigate = useNavigate();

    React.useEffect(() => {
        const token = localStorage.getItem("token")
        if (!token){
            navigate("/signin");
        }
    });

    return (
        <div className="container-fluid w-100 h-100 pt-3 mt-5 p-4 bg-dark text-danger">
            <InvestorGrid />
        </div>
    )
}

export default InvestorView;