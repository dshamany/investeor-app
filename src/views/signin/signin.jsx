import React from 'react';
import '../signup/signup'; // We use the same style as signup for consistency

import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import { useNavigate } from 'react-router-dom';
import { sha256 } from 'js-sha256';

import { authenticateUser, getUser } from '../../lib/api';
import Spinner from '../../components/spinner';

export const SignInView = () => {

    const [loading, setLoading] = React.useState(false);

    const emailRef = React.useRef();
    const passwordRef = React.useRef();

    const navigate = useNavigate();

    const handleAuth = (callback) => {
        const payload = {
            "email": emailRef.current.value,
            "password": sha256(passwordRef.current.value)
        }
        authenticateUser(payload, (authData) => {
            const token = authData["token"]
            localStorage.setItem("token", token);

            getUser(token, (userData) => {
                const user = userData["user"];
                localStorage.setItem("user", JSON.stringify(user));
                setLoading(false);
                navigate("/");
            });
        });
    };

    return (
        <div className="signup-view">
            {!loading && <Form>
                <h1>Sign In </h1>
                <Form.Group>
                    <Form.Control ref={emailRef} type="email" placeholder='Email Address' />
                    <Form.Control ref={passwordRef} type="password" placeholder='Password' />
                </Form.Group>
                <Form.Group className='action-bar'>
                    <Button variant="secondary" onClick={() => navigate("/signup")}>Sign Up</Button>
                    <Button onClick={() => {
                        setLoading(true);

                        if (!emailRef.current.value || !passwordRef.current.value) {
                            setLoading(false);
                            return;
                        }

                        handleAuth();
                    }}
                        type="button">Sign In</Button>
                </Form.Group>
            </Form>}
            {loading && <Spinner />}
        </div>
    )
}

export default SignInView;