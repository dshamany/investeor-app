import React from 'react';
import './signup.style.css'

import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import { useNavigate } from 'react-router-dom';

import { createUser } from '../../lib/api';
import { sha256 } from 'js-sha256';
import Spinner from '../../components/spinner';

export const SignUpView = () => {
    const [loading, setLoading] = React.useState(false);

    const fullnameRef = React.useRef();
    const emailRef = React.useRef();
    const passwordRef = React.useRef();

    const navigate = useNavigate();

    return (
        <div className="signup-view">
            {!loading && <Form>
                <h1>Sign Up </h1>
                <Form.Group>
                    <Form.Control ref={fullnameRef} type="text" placeholder='Full Name' />
                    <Form.Control ref={emailRef} type="email" placeholder='Email Address' />
                    <Form.Control ref={passwordRef} type="password" placeholder='Password' />
                </Form.Group>
                <Form.Group className='action-bar'>
                    <Button variant="secondary" onClick={() => navigate("/signin")}>Sign In</Button>
                    <Button onClick={() => {
                        setLoading(true);
                        createUser({
                            fullname: fullnameRef.current.value,
                            email: emailRef.current.value,
                            password: sha256(passwordRef.current.value)
                        },
                            (_ => {
                                setLoading(false);
                                navigate("/signin");
                            })
                        )
                    }} type="button">Sign Up</Button>
                </Form.Group>
            </Form>}
            {loading && <Spinner />}
        </div>
    )
}

export default SignUpView;