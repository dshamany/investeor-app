import FlatList from 'flatlist-react/lib';
import React from 'react';
import { useNavigate } from 'react-router-dom';
import CarouselView from '../../components/carousel';
import './home.style.css'

export const HomeView = () => {

    const navigate = useNavigate();

    React.useEffect(() => {
        const token = localStorage.getItem("token")
        if (!token) {
            navigate("/signin");
        }
    });

    return (
        <div className="container-fluid w-100 h-25 p-0 pt-5">
            <CarouselView />
            <div className='container-fluid w-100 h-100 bg-light pt-50' color="#fff">
                <div style={{ marginTop: 285 }}>
                    <FlatList
                        list={[{ title: "first" }, { title: "second" }, { title: "third" }, { title: "fourth" }, { title: "fifth" }, { title: "third" }, { title: "fourth" }, { title: "fifth" }]}
                        renderItem={(item) => <div style={{ backgroundColor: "#333", height: 250, width: "100vw", display: "flex", alignItems: "center", padding: 25, margin: "-13px", border: "20px solid black" }}><text style={{ color: "#fff" }}>{item.title}</text></div>}
                        renderWhenEmpt={() => <div><text>List is empty</text></div>}
                        // sortBy={["title"]}
                    />
                </div>
            </div>
        </div>
    )
}

export default HomeView;