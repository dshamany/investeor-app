import { useNavigate } from "react-router-dom"

export const dummySignin = () => {
    const nav = useNavigate();
    nav("/account")
}