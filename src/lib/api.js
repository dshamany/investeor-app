const BASE_URL = "https://heev9j5u26.execute-api.us-west-2.amazonaws.com";

export const createUser = async (payload, callback) => {
    const url = BASE_URL+"/user";
    await fetch(url, {
        method: "POST",
        headers: {
            "Content-type": "application/json; charset=UTF-8"
        },
        body: JSON.stringify(payload)
    })
    .then(data => data.json())
    .then(jsonData => {
        callback(jsonData);
    })
    .catch(error => console.log(error))
}

export const authenticateUser = async (payload, callback) => {
    const url = BASE_URL+"/user/auth";
    await fetch(url, {
        method: "POST",
        headers: {
            "Content-type": "application/json; charset=UTF-8"
        },
        body: JSON.stringify(payload)
    })
    .then(data => data.json())
    .then(jsonData => {
        callback(jsonData);
    })
    .catch(error => console.log(error))
}

export const getUser = async (token, callback) => {
    const url = BASE_URL+"/user";
    await fetch(url, {
        headers: {
            "Authorization": `Bearer ${token}`,
        }
    })
    .then(data => data.json())
    .then(jsonData => {
        callback(jsonData);
    })
    .catch(error => console.log(error))
}

export const updateUser = async (payload, callback) => {
    const url = BASE_URL+"/user";
    await fetch(url, {
        method: "PUT",
        headers: {
            "Content-type": "application/json; charset=UTF-8"
        },
        body: JSON.stringify(payload)
    })
    .then(data => data.json())
    .then(jsonData => {
        callback(jsonData);
    })
    .catch(error => console.log(error))
}