import React from 'react';

export const Spinner = () => (
    <div class="spinner-border text-light" role="status">
        <span class="sr-only"></span>
    </div>
)

export default Spinner;