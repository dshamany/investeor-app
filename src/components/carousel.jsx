import React from 'react';

import Carousel from 'react-bootstrap/Carousel';

export const CarouselView = () => {

    return (
        <div style={{ display: "block", padding: 30, width: "100%" }}>
            <Carousel style={{ position: "fixed", top: 50, left: 0, width: "100%", margin: 0, boxShadow: "0px 5px 15px #222", border: "0.5px solid #555" }} interval={3000}>
                <Carousel.Item>
                    <img className='d-block w-100' style={{ width: "100%", height: 330, objectFit: "fill" }} src="https://media.geeksforgeeks.org/wp-content/uploads/20210425122739/2-300x115.png" alt="one" />
                </Carousel.Item>
                <Carousel.Item>
                    <img className='d-block w-100' src="https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fimg1.wsimg.com%2Fisteam%2Fip%2F4937856d-7c53-4e53-a96f-f7db20e494f5%2FQuayChain%2520Logo.jpg&f=1&nofb=1&ipt=a590dcd9811d108326dd3bc7320f84304c0878da52dbf12f9e7544d26552f416&ipo=images" alt="two" />
                </Carousel.Item>
            </Carousel>
        </div>
    )
}

const styles = {
    item: {
        backgroundImage: "linear-gradient(180deg, #000, transparent)",
        backgroundColor: "black",
        zIndex: 1
    }
}

export default CarouselView;