import React from 'react';

export const InvestorGrid = () => {

    return (
        <div className="grid-container" style={styles.gridContainer}>
            <div className="row" style={{margin: 10, width: "100vw", maxWidth: 2000 }}>
            <div className="col-3" style={styles.gridElement}>Investor</div>
            <div className="col-3" style={styles.gridElement}>Investor</div>
            <div className="col-3" style={styles.gridElement}>Investor</div>
            <div className="col-3" style={styles.gridElement}>Investor</div>
            <div className="col-3" style={styles.gridElement}>Investor</div>
            <div className="col-3" style={styles.gridElement}>Investor</div>
            <div className="col-3" style={styles.gridElement}>Investor</div>
            <div className="col-3" style={styles.gridElement}>Investor</div>
            <div className="col-3" style={styles.gridElement}>Investor</div>
            <div className="col-3" style={styles.gridElement}>Investor</div>
            </div>
        </div>
    )
}

const styles = {
    gridContainer: {
        display: "flex",
        alignItems: "flex-start",
        justifyContent: "flex-start",
        flexWrap: "wrap",
        height: "100%",
        width: "100%",
        maxWidth: "2000px",
    },
    gridElement: {
        backgroundColor: "white",
        color: "black",
        border: "2px solid black",
        minHeight: 300,
        minWidth: 300,
        maxHeight: 600,
        maxWidth: 600,
        width: "100vw"
    }
}