import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import reportWebVitals from './reportWebVitals';
import 'bootstrap/dist/css/bootstrap.min.css';

// Router
import { createBrowserRouter, RouterProvider } from 'react-router-dom';

// Views
import HomeView from './views/home/home';
import SignInView from './views/signin/signin';
import SignUpView from './views/signup/signup';
import InvestorView from './views/investor/investor';
import StartupView from './views/startup/startup';
import TalentView from './views/talent/talent';

import Nav from './views/nav/nav';
import AccountView from './views/account/account';


const router = createBrowserRouter([
  {
    path: "/",
    element: <Nav><HomeView/></Nav>
  },
  {
    path: "/signin",
    element: <Nav><SignInView/></Nav>
  },
  {
    path: "/signup",
    element: <Nav><SignUpView/></Nav>
  },
  {
    path: "/investor",
    element: <Nav><InvestorView/></Nav>
  },
  {
    path: "/startup",
    element: <Nav><StartupView/></Nav>
  },
  {
    path: "/talent",
    element: <Nav><TalentView/></Nav>
  },
  {
    path: "/account",
    element: <Nav><AccountView/></Nav>
  },
])

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
      <RouterProvider router={router} />
  </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
